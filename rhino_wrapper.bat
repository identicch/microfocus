@echo off
setlocal

REM Define classpaths
set "SCRIPT_PATH=%~dp0"
set "RHINO_DIR=C:\PATH\TO\RHINO"
set "LIB_DIR=C:\PATH\TO\LIBS"

REM Set the classpath with Rhino JAR and additional libraries
set "CLASSPATH=%RHINO_DIR%\rhino-1.7.14.jar;%LIB_DIR%\httpclient-4.5.9.jar;%LIB_DIR%\commons-logging.jar"

java -cp "%CLASSPATH%" org.mozilla.javascript.tools.shell.Main %*
