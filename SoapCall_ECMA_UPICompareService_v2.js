// Example usage
// url, ahv, firstNames, officialName, Sex (1 = M, 2 = F), Birthday 
//callSoapService(url, ahvvn, firstNames, officialName, sex, dateOfBirth, senderId, recipientId, messageId, testDeliveryFlag, username, password);
//callSoapService("https://localhost:8443/wsproxy/services/UPICompareService2", "7568816108301", "Hanspeter", "Näf", 1, "1967-01-12", "T4-967813-3", "T3-CH-24", "1234" ,"true", "T4-967813-3", "strong_password");

// Import packages 
importPackage(org.apache.http.client);
importPackage(org.apache.http.client.methods);
importPackage(org.apache.http.entity);
importPackage(org.apache.http.impl.client);
importPackage(org.apache.http.message);
importPackage(org.apache.http.util);
importClass(java.util.Base64);  // check with Matthias if package is available

function callSoapService(url, ahvvn, firstNames, officialName, sex, dateOfBirth, senderId, recipientId, messageId, testDeliveryFlag, username, password) {
    try {
        var upiResponse = "";

        // Create HttpClient instance
        var client = HttpClientBuilder.create().build();

        // Set the SOAP endpoint URL
        var uri = new java.net.URI(url);

        // Create the SOAP request
        var request = new HttpPost(uri);

        // Ensure username and password are defined as strings for Basic Authentication
        var auth = username + ":" + password;

        // Convert auth to a Java string and encode it using Base64
        var authJavaString = new java.lang.String(auth); // Create a Java String from auth
        var encodedAuth = java.util.Base64.getEncoder().encodeToString(authJavaString.getBytes("UTF-8")); // Use getBytes on Java String

        // Add Authorization header with Basic Authentication
        request.addHeader("Authorization", "Basic " + encodedAuth);

        // Define the SOAP envelope XML string
        var soapEnvelope =
            "<soap:Envelope xmlns:soap='http://www.w3.org/2003/05/soap-envelope' xmlns:ns='http://www.ech.ch/xmlns/eCH-0086/2' xmlns:ns1='http://www.ech.ch/xmlns/eCH-0058/5' xmlns:ns2='http://www.ech.ch/xmlns/eCH-0044/4' xmlns:ns3='http://www.ech.ch/xmlns/eCH-0084/2' xmlns:ns4='http://www.ech.ch/xmlns/eCH-0021/7' xmlns:ns5='http://www.ech.ch/xmlns/eCH-0011/8'>" +
            "<soap:Header />" +
            "<soap:Body>" +
            "    <ns:request minorVersion='0'> " +
            "        <ns:header>" +
            "            <ns1:senderId>sedex://" + senderId + "</ns1:senderId>" +
            "            <ns1:recipientId>sedex://" + recipientId + "</ns1:recipientId>" +
            "            <ns1:messageId>" + messageId + "</ns1:messageId>" +
            "            <ns1:messageType>86</ns1:messageType>" +
            "            <ns1:sendingApplication>" +
            "                <ns1:manufacturer>KAIO</ns1:manufacturer>" +
            "                <ns1:product>KAIO</ns1:product>" +
            "                <ns1:productVersion>1</ns1:productVersion>" +
            "            </ns1:sendingApplication>" +
            "            <ns1:messageDate>2024-10-22T09:30:47</ns1:messageDate>" +
            "            <ns1:action>5</ns1:action>" +
            "            <ns1:testDeliveryFlag>" + testDeliveryFlag + "</ns1:testDeliveryFlag>" +
            "        </ns:header>" +
            "        <ns:content>" +
            "            <ns:responseLanguage>DE</ns:responseLanguage>" +
            "            <ns:dataToCompare>" +
            "                <ns:dataToCompareId>1</ns:dataToCompareId>" +
            "                <ns:vn>" + ahvvn + "</ns:vn>" +
            "                <ns:personToUpi>" +
            "                    <ns3:firstName>" + firstNames + "</ns3:firstName>" +
            "                    <ns3:officialName>" + officialName + "</ns3:officialName>" +
            "                    <ns3:sex>" + sex + "</ns3:sex>" +
            "                    <ns3:dateOfBirth>" +
            "                        <ns2:yearMonthDay>" + dateOfBirth + "</ns2:yearMonthDay>" +
            "                    </ns3:dateOfBirth>" +
            "                </ns:personToUpi>" +
            "            </ns:dataToCompare>" +
            "        </ns:content>" +
            "    </ns:request>" +
            "</soap:Body>" +
            "</soap:Envelope>";

        // Log the SOAP request
        print("SOAP Request: " + soapEnvelope);

        // Set the SOAP request body
        var entity = new StringEntity(soapEnvelope, "UTF-8");
        request.setEntity(entity);

        // Set Content-Type header
        request.addHeader("Content-Type", "application/soap+xml;charset=UTF-8");

        // Set other headers to match Postman
        request.addHeader("Accept", "*/*");
        request.addHeader("Connection", "keep-alive");

        // Execute the SOAP request
        var response = client.execute(request);

        // Log the SOAP response status code
        print("SOAP Response Status Code: " + response.getStatusLine().getStatusCode());

        // Process the SOAP response
        if (response.getStatusLine().getStatusCode() === 200) {
            var soapResponse = EntityUtils.toString(response.getEntity());

            // Log the SOAP response
            print("SOAP Response: " + soapResponse);

            // Check for identicalData in the SOAP response using flexible namespace matching
            var identicalDataRegex = /<ns.*?:identicalData>(.*?)<\/ns.*?:identicalData>/;
            var identicalMatch = identicalDataRegex.exec(soapResponse);
            var identicalData = identicalMatch ? identicalMatch[1] : null;

            // Check for differentData in the SOAP response using flexible namespace matching
            var differentDataRegex = /<ns.*?:differentData>(.*?)<\/ns.*?:differentData>/;
            var differentMatch = differentDataRegex.exec(soapResponse);

            if (identicalData === "true") {
                // Data is identical
                upiResponse = "Compared data is identical";
            } else if (differentMatch) {
                // Data is different, extract relevant fields using flexible namespace matching
                var firstNamesRegex = /<ns.*?:firstName>(.*?)<\/ns.*?:firstName>/;
                var officialNameRegex = /<ns.*?:officialName>(.*?)<\/ns.*?:officialName>/;
                var dateOfBirthRegex = /<ns.*?:yearMonthDay>(.*?)<\/ns.*?:yearMonthDay>/;
                var ahvvnRegex = /<ns.*?:activeVn>(.*?)<\/ns.*?:activeVn>/;

                var firstNames = firstNamesRegex.exec(soapResponse) ? firstNamesRegex.exec(soapResponse)[1] : null;
                var officialName = officialNameRegex.exec(soapResponse) ? officialNameRegex.exec(soapResponse)[1] : null;
                var dateOfBirth = dateOfBirthRegex.exec(soapResponse) ? dateOfBirthRegex.exec(soapResponse)[1] : null;
                var ahvvn = ahvvnRegex.exec(soapResponse) ? ahvvnRegex.exec(soapResponse)[1] : null;

                // Build the response message for different data
                upiResponse = "Compared data is different. Response from UPI, Official Name: " + officialName + ", First Names: " + firstNames + ", Birthday: " + dateOfBirth + ", AHV: " + ahvvn;
            } else {
                // Handle other cases
                upiResponse = "No matching data found in the response.";
            }

        } else {
            print("SOAP request failed with status " + response.getStatusLine().getStatusCode());
            return "SOAP request failed with status " + response.getStatusLine().getStatusCode();
        }

    } catch (e) {
        var errorMessage = e.toString();
        return "Error: " + errorMessage;
    }
    return upiResponse;
}