#!/bin/bash

# Define classpaths
SCRIPT_PATH=$(dirname "$0")
RHINO_DIR="/PATH/TO/RHINO"
LIB_DIR="/PATH/TO/LIBS"

# Set the classpath with Rhino JAR and additional libraries
CLASSPATH="$RHINO_DIR/rhino-1.7.14.jar:$LIB_DIR/org.apache.commons.httpclient.jar:$LIB_DIR/org-apache-commons-logging.jar:$LIB_DIR/commons-codec-1.16.0.jar"

java -cp "$CLASSPATH" org.mozilla.javascript.tools.shell.Main "$@"