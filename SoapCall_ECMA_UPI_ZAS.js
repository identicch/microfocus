// Example usage
//var url = "http://localhost:8080/wsproxy/services/UPICompareService";
// url, ahv, firstNames, officialName, Sex (1 = M, 2 = F), Birthday 
//callSoapService(url, "7568816108301", "Hanspete", "Näf", 1, "1967-01-12");
//callSoapService(http://localhost:8080/wsproxy/services/UPICompareService, "7568816108301", "Hanspete", "Näf", 1, "1967-01-12");

// Import packages 
importPackage(org.apache.http.client);
importPackage(org.apache.http.client.methods);
importPackage(org.apache.http.entity);
importPackage(org.apache.http.impl.client);
importPackage(org.apache.http.message);
importPackage(org.apache.http.util);

function callSoapService(url, ahvvn, firstNames, officialName, sex, dateOfBirth) {
    try {
        var upiResponse = "";

        // Create HttpClient instance
        var client = HttpClientBuilder.create().build();

        // Set the SOAP endpoint URL
        var uri = new java.net.URI(url);

        // Create the SOAP request
        var request = new HttpPost(uri);

        // Set SOAPAction header
        request.addHeader("SOAPAction", "http://soap.upi.admin.ch");

        // Set Content-Type header
        request.addHeader("Content-Type", "text/xml;charset=UTF-8");

        // Define the SOAP envelope XML string
        var soapEnvelope =
            "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns='http://www.ech.ch/xmlns/eCH-0086/1' xmlns:ns1='http://www.ech.ch/xmlns/eCH-0044/1' xmlns:ns2='http://www.ech.ch/xmlns/eCH-0084/1'>" +
            "    <soapenv:Header/>" +
            "    <soapenv:Body>" +
            "        <ns:compareDataRequest>" +
            "            <ns:exportDate>2023-07-06</ns:exportDate>" +
            "            <ns:dataToCompare>" +
            "                <ns:ahvvn>" + ahvvn + "</ns:ahvvn>" +
            "                <ns:firstNames>" + firstNames + "</ns:firstNames>" +
            "                <ns:officialName>" + officialName + "</ns:officialName>" +
            "                <ns:sex>" + sex + "</ns:sex>" +
            "                <ns:dateOfBirth>" +
            "                    <ns1:yearMonthDay>" + dateOfBirth + "</ns1:yearMonthDay>" +
            "                </ns:dateOfBirth>" +
            "            </ns:dataToCompare>" +
            "        </ns:compareDataRequest>" +
            "    </soapenv:Body>" +
            "</soapenv:Envelope>";

        // Set the SOAP request body
        request.setEntity(new StringEntity(soapEnvelope, "UTF-8"));

        // Execute the SOAP request
        var response = client.execute(request);

        // Process the SOAP response
        if (response.getStatusLine().getStatusCode() === 200) {
            var soapResponse = EntityUtils.toString(response.getEntity());

            // Define 3 possible response tags
            var identicalData = /<ns3:identicalData>/;
            var differentData = /<ns3:differentData>/;
            var refused = /<ns3:refused>/;

            if (identicalData.test(soapResponse)) {
                upiResponse = "Compared data is identical";
            } else if (differentData.test(soapResponse)) {

                // Define tags in response
                // First Names
                var firstNamesRegex = /<ns5:firstNames>(.*?)<\/ns5:firstNames>/;
                var match = firstNamesRegex.exec(soapResponse);
                var firstNames = match[1];

                // Official Name
                var officialNameRegex = /<ns5:officialName>(.*?)<\/ns5:officialName>/;
                match = officialNameRegex.exec(soapResponse);
                var officialName = match[1];

                // Birthday
                var dateOfBirthRegex = /<ns4:yearMonthDay>(.*?)<\/ns4:yearMonthDay>/;
                match = dateOfBirthRegex.exec(soapResponse);
                var dateOfBirth = match[1];

                // AHVN
                var ahvvnRegex = /<ns3:ahvvn>(.*?)<\/ns3:ahvvn>/;
                match = ahvvnRegex.exec(soapResponse);
                var ahvvn = match[1];

                upiResponse = "Compared data is different. Response from UPI, Official Name: " + officialName + ", First Names: " + firstNames + ", Birthday: " + dateOfBirth + ", AHV: " + ahvvn;
            } else if (refused.test(soapResponse)) {
                upiResponse = "Request was refused";
            }
        } else {
            return "SOAP request failed with status " + response.getStatusLine().getStatusCode();
        }

    } catch (e) {
        var errorMessage = e.toString();
        return "Error: " + errorMessage;
    }
    return upiResponse;
}

